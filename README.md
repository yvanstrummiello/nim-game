# nim_game

Nim game application developped for the cursus "Flutter Developper" from Purple Giraffe

## Details

This game use the class Player to manager player names.

When app launch, the home screen is used to give the players name. If no name is given a generic name "Anonymous" is provided.
The second screen is the Game screen, where players fights.
Then when a player win a congratulation screen is displayed, here player can choose to terminate game or to play again.