import 'package:flutter/material.dart';
import 'package:nim_game/Model/game_manager.dart';
import 'package:nim_game/winner.dart';

class Game extends StatefulWidget {
  final String player1;
  final String player2;

  const Game({Key? key, required this.player1, required this.player2})
      : super(key: key);

  @override
  _GameState createState() => _GameState();
}

class _GameState extends State<Game> {
  static const double _nameSize = 22.00;
  int _trombone = GameManager.trombone;
  final newGame = GameManager(true);

  Widget _generateElement(BuildContext context, int nbElement) {
    return Image.asset('assets/paperclip.png', height: 20);
  }

  _removeTrombone(int numberClicked) {
    newGame.playerTurn();
    
    setState(() {
      _trombone -= numberClicked;
      if (_trombone <= 0){
        _trombone = 0;
        _gameFinished();
      }
    });
  }

  _gameFinished(){
    if (newGame.isPlayerOneTurn){
      Navigator.push(context, MaterialPageRoute(
        builder:(context) => Winner(winner: widget.player1, looser: widget.player2),
      ));
    } else {
      Navigator.push(context, MaterialPageRoute(
        builder:(context) => Winner(winner: widget.player2, looser: widget.player1),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Jouez...."),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(widget.player1, style: const TextStyle(fontSize: _nameSize)),
            if (newGame.isPlayerOneTurn)
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                    onPressed: () => _removeTrombone(GameManager.oneButton),
                    child: Text(GameManager.oneButton.toString())),
                ElevatedButton(
                    onPressed: () => _removeTrombone(GameManager.twoButton),
                    child: Text(GameManager.twoButton.toString())),
                ElevatedButton(
                    onPressed: () => _removeTrombone(GameManager.threeButton),
                    child: Text(GameManager.threeButton.toString()))
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 40.00, bottom: 40.00),
              child: SizedBox(
                height: 384.00,
                child: GridView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 5,
                    ),
                    itemCount: _trombone,
                    itemBuilder: _generateElement),
              ),
            ),
            if (!newGame.isPlayerOneTurn)
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                    onPressed: () => _removeTrombone(GameManager.oneButton),
                    child: Text(GameManager.oneButton.toString())),
                ElevatedButton(
                    onPressed: () => _removeTrombone(GameManager.twoButton),
                    child: Text(GameManager.twoButton.toString())),
                ElevatedButton(
                    onPressed: () => _removeTrombone(GameManager.threeButton),
                    child: Text(GameManager.threeButton.toString())),
              ],
            ),
            Text(
              widget.player2,
              style: const TextStyle(fontSize: _nameSize),
            ),
          ],
        ),
      ),
    );
  }
}
