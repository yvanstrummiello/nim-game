import 'package:flutter/material.dart';
import 'package:nim_game/game.dart';
import 'package:nim_game/Model/player.dart';

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Player _player1 = Player("Anonymous 1");
  Player _player2 = Player("Anonymous 2");

  _savedPlayer1Name(String value){
    _player1.name = value;
  }
  _savedPlayer2Name(String value){
    _player2.name = value;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Center(
          child: Text("Accueil"))
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          // ignore: prefer_const_literals_to_create_immutables
          children:  [
            const Padding(
              padding: EdgeInsets.only(top: 25.00),
              child: Text("Bienvenue dans le jeu de NIM",
              style: TextStyle(
                fontSize: 20.00,
                letterSpacing: 2.00,
              ),),
            ),
            const Padding(
              padding: EdgeInsets.only(top: 50.00, bottom: 10.00),
              child: Text("Pseudo Joueur 1", 
              style: TextStyle(
                fontSize: 18.00),
                ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 25.00, right: 25.00),
              child: TextField(
                autocorrect: false,
                onChanged: _savedPlayer1Name,
                textAlign: TextAlign.center,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5.00))
                  )
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(top: 18.00, bottom: 8.00),
              child: Text("Pseudo Joueur 2", 
              style: TextStyle(
                fontSize: 18.00),
                ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 25.00, right: 25.00),
              child: TextField(
                autocorrect: false,
                onChanged: _savedPlayer2Name,
                textAlign: TextAlign.center,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5.00))
                  )
                ),
              ),
            ),
            const SizedBox(height: 40),
            ElevatedButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) => Game(player1: _player1.name, player2: _player2.name)
                ));
              }, 
              child: const Text("Démarrer la partie"))
          ],
        ),
        
      ),
    );
  }
}