import 'package:flutter/material.dart';
import 'package:nim_game/game.dart';
import 'package:nim_game/home.dart';

class Winner extends StatelessWidget {
  final String winner;
  final String looser;
  const Winner({Key? key, required this.winner, required this.looser})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 12.0, right: 12.0),
                child: Text(
                  "Félicitations $winner, vous avez gagné !!",
                  style: const TextStyle(fontSize: 16.00),
                ),
              ),
              const SizedBox(height: 80),
              Image.asset('assets/victory.png'),
              const SizedBox(height: 80),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  TextButton(
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) => Game(player1: winner, player2: looser),
                      ));
                    }, 
                    child: const Text("Rejouer", style: TextStyle(fontSize: 25.00),)),
                  TextButton(
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) => const Home(),
                      ));
                    }, 
                    child: const Text("Terminer", style: TextStyle(fontSize: 25.00),))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
//TODO TROUVER UNE IMAGE DE VICTOIRE !
