class GameManager {
  static const trombone = 20;
  static int get oneButton => 1;
  static int get twoButton => 2;
  static int get threeButton => 3;
  bool _isPlayerOneTurn;

  bool get isPlayerOneTurn => _isPlayerOneTurn;

  set isPlayerOneTurn(bool isPlayerOneTurn) {
    _isPlayerOneTurn = isPlayerOneTurn;
  }

  GameManager(this._isPlayerOneTurn);

  playerTurn(){
    if(_isPlayerOneTurn){
      _isPlayerOneTurn = false;
    } else {
      isPlayerOneTurn = true;
    }
  }
  
}